# Godot addons

A list of godot engine addons

Use Gitlab webIDE to edit this file, add suggestions and make a pull request. 

## c++ modules

require a compilation of the engine and therefore the re-generation of export templates

| name | description | address | git | status | tags | image |
|--|------|------|------|--|--|--|
| scatter | randomly fill an area with props or other scenes | https://github.com/HungryProton/scatter | git@github.com:HungryProton/scatter.git | not tested | generative | <img src="https://user-images.githubusercontent.com/52043844/68284290-7ca01780-007d-11ea-979b-128ca7038787.png" width="100"/>|
| futari | Contains classes to interacts with particle systems, such as wind, vortex and localised modifiers. | https://gitlab.com/polymorphcool/futari-addon | git@gitlab.com:polymorphcool/futari-addon.git | beta | physics, particles | <img src="https://polymorph.cool/wp-content/uploads/2019/04/futari-addon-demo-walls-1024x576.jpg" width="100"/>|
| softskin | Tensegrity addon for godot engine | https://gitlab.com/frankiezafe/softskin | git@gitlab.com:frankiezafe/softskin.git | wip | physics | |
| gOSC | OSC module for godot engine | https://gitlab.com/frankiezafe/gosc | git@gitlab.com:frankiezafe/gosc.git | stable | network | |
| gSyphon | Syphon addon for godot engine | https://gitlab.com/frankiezafe/gsyphon | git@gitlab.com:frankiezafe/gsyphon.git | video | stable | |
| Oculus Quest Toolkit | An easy to use VR toolkit for Oculus Quest development using the Godot game engine | https://github.com/NeoSpark314/godot_oculus_quest_toolkit | git@github.com:NeoSpark314/godot_oculus_quest_toolkit.git | VR | not tested | <img src="https://raw.githubusercontent.com/NeoSpark314/godot_oculus_quest_toolkit/master/doc/images/feature_overview.jpg" width="100"/>|

## thirdparty to integrate

list of interesting libraries to port into godot

| name | description | address | git | status | tags | image |
|--|------|------|------|--|--|--|
| HAP | A codec for fast video playback (unity plugin: https://github.com/keijiro/KlakHap) | https://github.com/Vidvox/hap | git@github.com:Vidvox/hap.git | stable | video | |
| Spout2 | A video frame sharing system for Microsoft Windows | https://github.com/leadedge/Spout2 | git@github.com:leadedge/Spout2.git | stable | video | |
| FEMFX | multithreaded CPU library for deformable material physics, [wiki](https://gpuopen.com/gaming-product/femfx/) | https://github.com/GPUOpen-Effects/FEMFX | git@github.com:GPUOpen-Effects/FEMFX.git | not tested | physics | <img src="https://gpuopen.com/wp-content/uploads/2019/12/tires.gif" width="100"/>|


## gdnative

ready to use in a standard godot project

| name | description | address | git | status | tags | image |
|--|------|------|------|--|--|--|
| gdnative_template | Simplified project to start a gdnative module for Godot Engine. | https://gitlab.com/frankiezafe/gdnative_template | git@gitlab.com:frankiezafe/gdnative_template.git | stable | dev | |
| godot-sqlite | Makes SQLite3 available in Godot 3.1+ | https://github.com/2shady4u/godot-sqlite | | database | |

## gdscript / csharp

set of scripts

| name | description | address | git | status | tags | image |
|--|------|------|------|--|--|--|
| UDPpingpong | shows how to send and receive UDP messages | https://gitlab.com/polymorphcool/udppingpong | git@gitlab.com:polymorphcool/udppingpong.git | stable | network |  |
| gtoolbox | customisation of godot engine for polymorph projects | https://gitlab.com/polymorphcool/gtoolbox | git@gitlab.com:polymorphcool/gtoolbox.git | wip | fx, shader, UI |  |
| godot3_procgen_demos | Exploring Procedural Generation algorithms in Godot | https://github.com/kidscancode/godot3_procgen_demos | git@github.com:kidscancode/godot3_procgen_demos.git | wip | generative |  |
| Wireframe | meshes and scripts to generate wireframe meshes in godot engine | https://gitlab.com/frankiezafe/godot-wireframe | git@gitlab.com:frankiezafe/godot-wireframe.git | wip | wireframe |  |
| MIDI Input example | Example of MIDI Input handling (e.g keyboard/controller) for the Godot game engine. | https://github.com/follower/godot-midi-input-example | git@github.com:follower/godot-midi-input-example.git | not tested | network | |
| Material Spray | small addon for the Godot engine (version 3.1.1 only) that can be used to paint meshes | https://github.com/RodZill4/godot-material-spray | git@github.com:RodZill4/godot-material-spray.git | not tested | mesh, edition | <img src="https://raw.githubusercontent.com/RodZill4/godot-material-spray/master/addons/material_spray/doc/screenshot.png" width="100"/>| 
| WaterPack | Godot water package for godot 3.1 | https://github.com/ywaby/godot-WaterPack | git@github.com:ywaby/godot-WaterPack.git | wip | shader, water | <img src="https://raw.githubusercontent.com/ywaby/godot-WaterPack/master/doc/screen_shot/ocean.jpg" width="100"/>|
| Settings Manager | Easy game settings management | https://github.com/Calinou/godot-settings-manager | | | |

```
template
| name | description | address | git | not_tested | tag | image |
```
